/**************************************************************************
* Copyright (C) 2017 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file Image.h
* @author GORAND Charles
*
*/

#ifndef GX_IMAGE_H
#define GX_IMAGE_H

#include "gx/core/Serializable.h"

namespace gx
{
class Image: public Serializable
{
	public :
		Image();
		virtual ~Image();

		enum eFormat
		{
			GRAY_SCALE = 0x01,
			RGB_565    = 0x02,
			RGB_888    = 0x03,
			RGBA_8888  = 0x04
		};
		
		enum eResult
		{
			SUCCESS,
			NO_MEMORY,
			FAILED
		};
		
		enum eCompressAlgorithm
		{
			NONE,
			BEST,
			LZW,
			RLE_PLAIN,
			RLE_LZW
		};

		uint32_t getWidth() const;
		uint32_t getHeight() const;
		eFormat getFormat() const;
		uint8_t getBpp() const;
		uint8_t *getSurface() const;
		
		void setCompressAlgorithm(eCompressAlgorithm c);
		eCompressAlgorithm getCompressAlgorithm() const;

		eResult allocate(uint32_t width, uint32_t height, eFormat f);

		virtual void serialize(gx::Serializer & s) const;
		virtual void deserialize(const gx::Serializer & s);

	private:
		void saveImageHeader(eCompressAlgorithm c, gx::Serializer &s) const;
		void serializeNone(gx::Serializer &s) const;
		bool serializeBest(gx::Serializer &s) const;
		void serializeLZW(gx::Serializer &s) const;
		void serializeRLE(gx::Serializer &s) const;
		void serializeRLELZW(gx::Serializer &s) const;
		void serializeRLE_8bits(gx::Serializer &s) const;
		void serializeRLE_565(gx::Serializer &s) const;
		
		eCompressAlgorithm getImageHeader(const gx::Serializer &s);
		void deserializeNone(const gx::Serializer &s);
		void deserializeLZW(const gx::Serializer &s);
		void deserializeRLE(const gx::Serializer &s);
		void deserializeRLELZW(const gx::Serializer &s);
		void deserializeRLE_8bits(const gx::Serializer &s);
		void deserializeRLE_565(const gx::Serializer &s);

		void filterSurfaceRGBA() const;
		void unfilterSurfaceRGBA() const;

		void filterSurfaceRGB() const;
		void unfilterSurfaceRGB() const;

	private:
		uint8_t            *m_pixels;
		uint32_t            m_width;
		uint32_t            m_height;
		eFormat             m_format;
		uint8_t             m_bpp;
		eCompressAlgorithm  m_compAlgo;
};
};

#endif // ndef GX_IMAGE_H

