/**************************************************************************
* Copyright (C) 2018 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file ImageConverter565.cpp
* @author GORAND Charles
*
*/

#include "ImageConverter565.h"

#include "gx/gfx/dither.h"

#ifndef GX_GFX_IMAGE_ENABLE_DITHER
#define GX_GFX_IMAGE_ENABLE_DITHER 1
#endif // ndef GX_GFX_IMAGE_ENABLE_DITHER

gx::Image *gx::ImageConverter565::fromRGBA(const gx::Image *src)
{
	Image              *dst;
	gx::Image::eResult  imgResult;
	uint8_t            *srcSurf;
	uint16_t           *dstSurf;
	uint16_t            r;
	uint16_t            g;
	uint16_t            b;
	uint32_t            h;
	uint32_t            w;
	uint32_t           ihx;
	uint32_t           ohx;
	uint32_t           iwx;
	uint32_t           owx;
	uint16_t           px;


	dst = nullptr;

	if (src->getFormat() != gx::Image::RGBA_8888)
	{
		return nullptr;
	}

	dst = new Image();

	imgResult = dst->allocate(src->getWidth(), src->getHeight(), gx::Image::RGB_565);

	if (imgResult != gx::Image::SUCCESS)
	{
		delete dst;
		return nullptr;
	}

	srcSurf = src->getSurface();
	dstSurf = (uint16_t*)dst->getSurface();

	for (h = 0 ; h < src->getHeight() ; h++)
	{
		ihx = src->getWidth() * h * src->getBpp();
		ohx = dst->getWidth() * h;

		for (w = 0; w < src->getWidth(); w++)
		{
			iwx = ihx + w * src->getBpp();
			owx = ohx + w;
			r = srcSurf[iwx++];
			g = srcSurf[iwx++];
			b = srcSurf[iwx++];
#if (defined(GX_GFX_IMAGE_ENABLE_DITHER) && GX_GFX_IMAGE_ENABLE_DITHER==1)
			px = gx_dither_xy(w, h, r, g, b);
#else
			px = ((r >> 3) << 11) | ((g >> 2) << 5) | (b >> 3);
#endif // def GX_GFX_IMAGE_ENABLE_DITHER

			dstSurf[owx] = px;
		}
	}

	return dst;
}

gx::Image *gx::ImageConverter565::toRGBA(const gx::Image *src)
{
	Image              *dst;
	gx::Image::eResult  imgResult;
	size_t              sz;
	uint16_t           *srcSurf;
	uint8_t            *dstSurf;

	dst = nullptr;

	if (src->getFormat() != gx::Image::RGB_565)
	{
		return nullptr;
	}

	dst = new Image();

	imgResult = dst->allocate(src->getWidth(), src->getHeight(), gx::Image::RGBA_8888);

	if (imgResult != gx::Image::SUCCESS)
	{
		delete dst;
		return nullptr;
	}

	sz = src->getWidth() * src->getHeight();
	srcSurf = (uint16_t*) src->getSurface();
	dstSurf = dst->getSurface();

	for (uint32_t i = 0; i < sz; i++)
	{
		dstSurf[i*dst->getBpp()]   = ((srcSurf[i] >> 11) << 3)&0xFF;
		dstSurf[i*dst->getBpp()+1] = ((srcSurf[i] >> 5)  << 2)&0xFF;
		dstSurf[i*dst->getBpp()+2] = ((srcSurf[i]) << 3)&0xFF;
		dstSurf[i*dst->getBpp()+3] = 0xFF;
	}

	return dst;
}

void gx::ImageConverter565::fromRGB(const gx::Image *src, gx::Image *dst)
{
	gx::Image::eResult  imgResult;
	uint8_t            *srcSurf;
	uint16_t           *dstSurf;
	uint16_t            r;
	uint16_t            g;
	uint16_t            b;
	uint32_t            h;
	uint32_t            w;
	uint32_t           ihx;
	uint32_t           ohx;
	uint32_t           iwx;
	uint32_t           owx;
	uint16_t           px;


	if (src->getFormat() != gx::Image::RGB_888)
	{
		return;
	}

	imgResult = dst->allocate(src->getWidth(), src->getHeight(), gx::Image::RGB_565);

	if (imgResult != gx::Image::SUCCESS)
	{
		delete dst;
		return;
	}

	srcSurf = src->getSurface();
	dstSurf = (uint16_t*)dst->getSurface();

	for (h = 0 ; h < src->getHeight() ; h++)
	{
		ihx = src->getWidth() * h * src->getBpp();
		ohx = dst->getWidth() * h;

		for (w = 0; w < src->getWidth(); w++)
		{
			iwx = ihx + w * src->getBpp();
			owx = ohx + w;
			r = srcSurf[iwx++];
			g = srcSurf[iwx++];
			b = srcSurf[iwx++];
			// NOTE : No dither in that case, Image was already dithered
			// fromRGB is only used by Image.cpp
// 			px = gx_dither_xy(w, h, r, g, b);
			px = ((r >> 3) << 11) | ((g >> 2) << 5) | (b >> 3);

			dstSurf[owx] = px;
		}
	}
}

void gx::ImageConverter565::toRGB(const gx::Image *src,gx::Image *dst)
{
	gx::Image::eResult  imgResult;
	size_t              sz;
	uint16_t           *srcSurf;
	uint8_t            *dstSurf;

	if (src->getFormat() != gx::Image::RGB_565)
	{
		return;
	}

	imgResult = dst->allocate(src->getWidth(), src->getHeight(), gx::Image::RGB_888);

	if (imgResult != gx::Image::SUCCESS)
	{
		delete dst;
		return;
	}

	sz = src->getWidth() * src->getHeight();
	srcSurf = (uint16_t*) src->getSurface();
	dstSurf = dst->getSurface();

	for (uint32_t i = 0; i < sz; i++)
	{
		dstSurf[i*dst->getBpp()]   = ((srcSurf[i] >> 11) << 3)&0xFF;
		dstSurf[i*dst->getBpp()+1] = ((srcSurf[i] >> 5)  << 2)&0xFF;
		dstSurf[i*dst->getBpp()+2] = ((srcSurf[i]) << 3)&0xFF;
		dstSurf[i*dst->getBpp()+3] = 0xFF;
	}
}


