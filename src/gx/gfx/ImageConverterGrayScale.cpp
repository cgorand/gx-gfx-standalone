/**************************************************************************
* Copyright (C) 2018 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file ImageConverterGrayScale.cpp
* @author GORAND Charles
*
*/

#include "ImageConverterGrayScale.h"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

gx::Image *gx::ImageConverterGrayScale::fromRGBA(const gx::Image *src)
{
	Image              *dst;
	gx::Image::eResult  imgResult;
	size_t              sz;
	uint8_t            *srcSurf;
	uint8_t            *dstSurf;
	uint32_t            value;
	uint32_t            r;
	uint32_t            g;
	uint32_t            b;

	dst = nullptr;

	if (src->getFormat() != gx::Image::RGBA_8888)
	{
		return nullptr;
	}

	dst = new Image();

	imgResult = dst->allocate(src->getWidth(), src->getHeight(), gx::Image::GRAY_SCALE);

	if (imgResult != gx::Image::SUCCESS)
	{
		delete dst;
		return nullptr;
	}

	sz = src->getWidth() * src->getHeight();
	srcSurf = src->getSurface();
	dstSurf = dst->getSurface();

	for (uint32_t i = 0; i < sz; i++)
	{
		r = srcSurf[i * src->getBpp()];
		g = srcSurf[i * src->getBpp() + 1];
		b = srcSurf[i * src->getBpp() + 2];
		// Average
		// Gray = (Red + Green + Blue) / 3
		// value = (srcSurf[i * src->getBpp()] + srcSurf[i * src->getBpp() + 1] + srcSurf[i * src->getBpp() + 2])/3;
		// luminance
		// Gray = (Red * 0.3 + Green * 0.59 + Blue * 0.11)
		// value = r*30/100 + g*59/100 + b*11/100;
		// Desaturation
		// Gray = ( Max(Red, Green, Blue) + Min(Red, Green, Blue) ) / 2
		// value = (MAX(r, MAX(g,b)) + MIN(r,MIN(g,b)))>> 1;

		value = r*30/100 + g*59/100 + b*11/100;
		dstSurf[i * dst->getBpp()] = value;
	}

	return dst;

}

gx::Image *gx::ImageConverterGrayScale::toRGBA(const gx::Image *src)
{
		Image              *dst;
	gx::Image::eResult  imgResult;
	size_t              sz;
	uint8_t            *srcSurf;
	uint8_t            *dstSurf;

	dst = nullptr;

	if (src->getFormat() != gx::Image::GRAY_SCALE)
	{
		return nullptr;
	}

	dst = new Image();

	imgResult = dst->allocate(src->getWidth(), src->getHeight(), gx::Image::RGBA_8888);

	if (imgResult != gx::Image::SUCCESS)
	{
		delete dst;
		return nullptr;
	}

	sz = src->getWidth() * src->getHeight();
	srcSurf = src->getSurface();
	dstSurf = dst->getSurface();

	for (uint32_t i = 0; i < sz; i++)
	{
		dstSurf[i*dst->getBpp()] = srcSurf[i];
		dstSurf[i*dst->getBpp()+1] = srcSurf[i];
		dstSurf[i*dst->getBpp()+2] = srcSurf[i];
		dstSurf[i*dst->getBpp()+3] = 0xFF;
	}

	return dst;
}

