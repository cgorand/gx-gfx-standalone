/**************************************************************************
* Copyright (C) 2018 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file ImageConverter565.h
* @author GORAND Charles
*
*/

#ifndef GX_IMAGECONVERTER565_H
#define GX_IMAGECONVERTER565_H

#include "gx/gfx/Image.h"

namespace gx
{
class ImageConverter565
{
	public:
		static Image* fromRGBA(const Image *src);
		static Image* toRGBA(const Image *src);

	private:
		friend class Image;
		static void fromRGB(const Image *src, Image *dst);
		static void toRGB(const Image *src, Image *dst);
};
};

#endif // ndef GX_IMAGECONVERTER565_H

