/**************************************************************************
* Copyright (C) 2018 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either expdsts or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file ImageConverterRGB.cpp
* @author GORAND Charles
*
*/

#include "ImageConverterRGB.h"

gx::Image *gx::ImageConverterRGB::fromRGBA(const gx::Image *src)
{
	Image              *dst;
	gx::Image::eResult  imgResult;
	size_t              sz;
	uint8_t            *srcSurf;
	uint8_t            *dstSurf;

	dst = nullptr;

	if (src->getFormat() != gx::Image::RGBA_8888)
	{
		return nullptr;
	}

	dst = new Image();

	imgResult = dst->allocate(src->getWidth(), src->getHeight(), gx::Image::RGB_888);

	if (imgResult != gx::Image::SUCCESS)
	{
		delete dst;
		return nullptr;
	}

	sz = src->getWidth() * src->getHeight();
	srcSurf = src->getSurface();
	dstSurf = dst->getSurface();

	for (uint32_t i = 0; i < sz; i++)
	{
		dstSurf[i*dst->getBpp()] = srcSurf[i*src->getBpp()];
		dstSurf[i*dst->getBpp()+1] = srcSurf[i*src->getBpp()+1];
		dstSurf[i*dst->getBpp()+2] = srcSurf[i*src->getBpp()+2];
	}

	return dst;
}

gx::Image *gx::ImageConverterRGB::toRGBA(const gx::Image *src)
{
	Image              *dst;
	gx::Image::eResult  imgResult;
	size_t              sz;
	uint8_t            *srcSurf;
	uint8_t            *dstSurf;

	dst = nullptr;

	if (src->getFormat() != gx::Image::RGB_888)
	{
		return nullptr;
	}

	dst = new Image();

	imgResult = dst->allocate(src->getWidth(), src->getHeight(), gx::Image::RGBA_8888);

	if (imgResult != gx::Image::SUCCESS)
	{
		delete dst;
		return nullptr;
	}

	sz = src->getWidth() * src->getHeight();
	srcSurf = src->getSurface();
	dstSurf = dst->getSurface();

	for (uint32_t i = 0; i < sz; i++)
	{
		dstSurf[i*dst->getBpp()] = srcSurf[i*src->getBpp()];
		dstSurf[i*dst->getBpp()+1] = srcSurf[i*src->getBpp()+1];
		dstSurf[i*dst->getBpp()+2] = srcSurf[i*src->getBpp()+2];
		dstSurf[i*dst->getBpp()+3] = 0xFF;
	}

	return dst;
}

