/**************************************************************************
* Copyright (C) 2017 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file Image.cpp
* @author GORAND Charles
*
*/

#include "Image.h"

#include "gx/gfx/ImageConverter565.h"
#include "gx/lzw/Lzw.h"

#define GX_LOG_TAG "Image"
#include "gx/core/Log.h"

#define IMAGE_FLAG_SMALL  0x01
#define IMAGE_FLAG_MEDIUM 0x02
#define IMAGE_FLAG_LARGE  0x04

#define IMAGE_FLAG_COMP_NONE       0x10
#define IMAGE_FLAG_COMP_LZW        0x20
#define IMAGE_FLAG_COMP_RLE_PLAIN  0x40
#define IMAGE_FLAG_COMP_RLE_LZW    0x80

gx::Image::Image()
{
	m_pixels   = nullptr;
	m_width    = 0;
	m_height   = 0;
	m_format   = gx::Image::RGB_565;
	m_bpp      = 2;
	m_compAlgo = BEST;
}

gx::Image::~Image()
{
	if (m_pixels)
	{
		delete m_pixels;
	}
}

uint8_t gx::Image::getBpp() const
{
	return m_bpp;
}

gx::Image::eFormat gx::Image::getFormat() const
{
	return m_format;
}

uint32_t gx::Image::getHeight() const
{
	return m_height;
}

uint32_t gx::Image::getWidth() const
{
	return m_width;
}

gx::Image::eResult gx::Image::allocate(uint32_t width, uint32_t height, gx::Image::eFormat f)
{
	if (m_pixels)
	{
		delete m_pixels;
	}

	m_pixels = nullptr;

	m_width  = width;
	m_height = height;
	m_format = f;

	switch (f)
	{
		case gx::Image::GRAY_SCALE:
		{
			m_bpp = 1;
			break;
		}

		case gx::Image::RGB_565:
		{
			m_bpp = 2;
			break;
		}

		case gx::Image::RGB_888:
		{
			m_bpp = 3;
			break;
		}

		case gx::Image::RGBA_8888:
		{
			m_bpp = 4;
			break;
		}
	}

	m_pixels = new uint8_t [m_width * m_height * m_bpp];

	if (!m_pixels)
	{
		m_width  = 0;
		m_height = 0;
		m_bpp    = 0;
		return gx::Image::NO_MEMORY;
	}

	return gx::Image::SUCCESS;
}

uint8_t *gx::Image::getSurface() const
{
	return m_pixels;
}

gx::Image::eCompressAlgorithm gx::Image::getCompressAlgorithm() const
{
	return m_compAlgo;
}

void gx::Image::setCompressAlgorithm(gx::Image::eCompressAlgorithm c)
{
	m_compAlgo = c;
}



void gx::Image::serialize(gx::Serializer &s) const
{
	bool isConverted = false;

	if (m_pixels)
	{
		switch (m_compAlgo)
		{
			case NONE:
			{
				serializeNone(s);
				break;
			}

			case BEST:
			{
				isConverted = serializeBest(s);
				break;
			}

			case LZW:
			{
				serializeLZW(s);
				break;
			}

			case RLE_PLAIN:
			{
				serializeRLE(s);
				break;
			}

			case RLE_LZW:
			{
				serializeRLELZW(s);
				break;
			}
		}

		if (isConverted)
		{
			s.writeByte(1);
		}
		else
		{
			s.writeByte(0);
		}
	}
	else
	{
		GX_ERROR("Image not allocated");
	}
}

void gx::Image::deserialize(const gx::Serializer &s)
{
	eCompressAlgorithm c;

	c = getImageHeader(s);

	switch (c)
	{
		case NONE:
		{
			deserializeNone(s);
			break;
		}

		case LZW:
		{
			deserializeLZW(s);
			break;
		}

		case RLE_PLAIN:
		{
			deserializeRLE(s);
			break;
		}

		case RLE_LZW:
		{
			deserializeRLELZW(s);
			break;
		}

		default:
		{
			GX_LOG("Unhandled case");
		}
	}

	if (s.readByte())
	{
		Image *converted565;

		converted565 = nullptr;

		if (m_format != RGB_888)
		{
			GX_FATAL("Not expected : %i",m_format);
		}

		converted565 = new Image();
		if (converted565)
		{
			ImageConverter565::fromRGB(this, converted565);
		}
		
		if (converted565)
		{
			m_pixels = converted565->m_pixels;
			m_bpp    = converted565->m_bpp;
			m_format = converted565->m_format;
			converted565->m_pixels = nullptr;
			delete converted565;
		}
	}
}


void gx::Image::saveImageHeader(eCompressAlgorithm c, gx::Serializer &s) const
{
	uint8_t compFlag;

	compFlag = 0;

	switch (c)
	{
		case NONE:
		{
			compFlag |= IMAGE_FLAG_COMP_NONE;
			break;
		}

		case LZW:
		{
			compFlag |= IMAGE_FLAG_COMP_LZW;
			break;
		}

		case RLE_PLAIN:
		{
			compFlag |= IMAGE_FLAG_COMP_RLE_PLAIN;
			break;
		}

		case RLE_LZW:
		{
			compFlag |= IMAGE_FLAG_COMP_RLE_LZW;
			break;
		}

		default:
		{
			GX_FATAL("Unhandled case");
		}
	}

	if ((getWidth() < 0xFF) && (getHeight() < 0xFF))
	{
		s.writeByte(compFlag | IMAGE_FLAG_SMALL);
		s.writeByte(getWidth());
		s.writeByte(getHeight());
	}
	else if ((getWidth() < 0xFFFF) && (getHeight() < 0xFFFF))
	{
		s.writeByte(compFlag | IMAGE_FLAG_MEDIUM);
		s.writeShort(getWidth());
		s.writeShort(getHeight());
	}
	else
	{
		s.writeByte(compFlag | IMAGE_FLAG_LARGE);
		s.writeInt32(getWidth());
		s.writeInt32(getHeight());
	}

	s.writeByte(m_format);
}


void gx::Image::serializeNone(gx::Serializer &s) const
{
	saveImageHeader(NONE, s);
	s.write(m_pixels, m_width * m_height * m_bpp);
}

bool gx::Image::serializeBest(gx::Serializer &s) const
{
	Serializer  m1;
	Serializer  m2;
	Serializer  m3;
	Serializer  m4;
	Serializer  m565;
	Serializer *mr;
	size_t      s1;
	size_t      s2;
	size_t      s3;
	size_t      s4;
	size_t      sr;
	bool        isConverted;

	isConverted = false;

	serializeNone(m1);
	serializeLZW(m2);
	serializeRLE(m3);
	serializeRLELZW(m4);

	s1 = m1.getSize();
	s2 = m2.getSize();
	s3 = m3.getSize();
	s4 = m4.getSize();

	mr = &m1;
	sr = s1;

	if (sr > s2)
	{
		sr = s2;
		mr = &m2;
	}

	if (sr > s3)
	{
		sr = s3;
		mr = &m3;
	}

	if (sr > s4)
	{
		sr = s4;
		mr = &m4;
	}

	// Try to get better result in RGB_888
	if (m_format == RGB_565)
	{
		Image  *convertedRGB;
		size_t  s565;

		convertedRGB  = new Image;
		
		if (convertedRGB)
		{
			gx::ImageConverter565::toRGB(this, convertedRGB);

			convertedRGB->setCompressAlgorithm(gx::Image::BEST);
			convertedRGB->serializeBest(m565);

			s565 = m565.getSize();

			if (sr > s565)
			{
				sr = s565;
				mr = &m565;
				isConverted = true;
			}

			delete convertedRGB;
		}
	}

	s.write(mr->getData(), mr->getSize());
	return isConverted;
}

void gx::Image::serializeLZW(gx::Serializer &s) const
{
	gx::Lzw lzw;
	saveImageHeader(LZW, s);

	switch (m_format)
	{
		case GRAY_SCALE:
		case RGB_565:
		{
			// No filter
			break;
		}

		case RGB_888:
		{
			filterSurfaceRGB();
			break;
		}

		case RGBA_8888:
		{
			filterSurfaceRGBA();
			break;
		}
	}

	lzw.compressTo(gx::Lzw::ULTRA, m_pixels, m_width * m_height * m_bpp, s);

	switch (m_format)
	{
		case GRAY_SCALE:
		case RGB_565:
		{
			// No filter
			break;
		}

		case RGB_888:
		{
			unfilterSurfaceRGB();
			break;
		}

		case RGBA_8888:
		{
			unfilterSurfaceRGBA();
			break;
		}
	}
}

void gx::Image::serializeRLE(gx::Serializer &s) const
{
	saveImageHeader(RLE_PLAIN, s);

	switch (m_format)
	{
		case gx::Image::GRAY_SCALE:
		case gx::Image::RGB_888:
		case gx::Image::RGBA_8888:
		{
			serializeRLE_8bits(s);
			break;
		}

		case gx::Image::RGB_565:
		{
			serializeRLE_565(s);
			break;
		}
	}
}

void gx::Image::serializeRLE_8bits(gx::Serializer &s) const
{
	uint32_t sz;
	uint8_t  b;
	uint8_t  count;

	sz = m_width * m_height;

	for (uint32_t c = 0; c < m_bpp; c++)
	{
		for (uint32_t i = 0; i < sz; i++)
		{
			b = m_pixels[i * m_bpp + c];
			count = 1;

			while ((i + 1 < sz) && (b == m_pixels[(i + 1)*m_bpp + c]) && (count < 0xFF))
			{
				count ++;
				i++;
			}

			if ((count < 4) && (b != 'x'))
			{
				// Only 3 or less occurence save it as is
				for (uint32_t j = 0; j < count; j++)
				{
					s.writeByte(b);
				}
			}
			else
			{
				// Several occurence or special 'x'
				s.writeByte('x');
				s.writeByte(count);
				s.writeByte(b);
			}
		}
	}
}

void gx::Image::serializeRLE_565(gx::Serializer &s) const
{
	uint32_t  sz;
	uint16_t  b;
	uint8_t   count;
	uint16_t *surf;

	surf = (uint16_t *)m_pixels;
	sz   = m_width * m_height;

	for (uint32_t i = 0; i < sz; i++)
	{
		b = surf[i];
		count = 1;

		while ((i + 1 < sz) && (b == surf[(i + 1)]) && (count < 0xFF))
		{
			count ++;
			i++;
		}

		if ((count < 4) && (b != 'x'))
		{
			// Only 3 or less occurence save it as is
			for (uint32_t j = 0; j < count; j++)
			{
				s.writeShort(b);
			}
		}
		else
		{
			// Several occurence or special 'x'
			s.writeShort('x');
			s.writeByte(count);
			s.writeShort(b);
		}
	}
}


void gx::Image::serializeRLELZW(gx::Serializer &s) const
{
	gx::Serializer tmp;
	gx::Lzw        lzw;

	saveImageHeader(RLE_LZW, s);

	// RLE First
	switch (m_format)
	{
		case gx::Image::GRAY_SCALE:
		case gx::Image::RGB_888:
		case gx::Image::RGBA_8888:
		{
			serializeRLE_8bits(tmp);
			break;
		}

		case gx::Image::RGB_565:
		{
			serializeRLE_565(tmp);
			break;
		}

		default:
		{
			GX_ERROR("Unhandled case");
		}
	}

	// Adding size
	s.writeInt32(tmp.getSize());

	if (tmp.getSize())
	{
		// LZW Compress
		lzw.compressTo(gx::Lzw::ULTRA, tmp.getData(), tmp.getSize(), s);
	}
}



gx::Image::eCompressAlgorithm gx::Image::getImageHeader(const gx::Serializer &s)
{
	uint8_t flag;
	eCompressAlgorithm c;

	flag = s.readByte();

	if (flag & IMAGE_FLAG_SMALL)
	{
		m_width = s.readByte();
		m_height = s.readByte();
	}
	else if (flag & IMAGE_FLAG_MEDIUM)
	{
		m_width = s.readShort();
		m_height = s.readShort();
	}
	else if (flag & IMAGE_FLAG_LARGE)
	{
		m_width = s.readInt32();
		m_height = s.readInt32();
	}

	if (flag & IMAGE_FLAG_COMP_NONE)
	{
		c = gx::Image::NONE;
	}
	else if (flag & IMAGE_FLAG_COMP_LZW)
	{
		c = gx::Image::LZW;
	}
	else if (flag & IMAGE_FLAG_COMP_RLE_PLAIN)
	{
		c = gx::Image::RLE_PLAIN;
	}
	else if (flag & IMAGE_FLAG_COMP_RLE_LZW)
	{
		c = gx::Image::RLE_LZW;
	}
	else
	{
		c = gx::Image::NONE;
		GX_ERROR("Unsupported compression flag");
	}

	m_format = (eFormat)s.readByte();

	allocate(m_width, m_height, m_format);

	return c;
}

void gx::Image::deserializeNone(const gx::Serializer &s)
{
	s.read(m_pixels, m_width * m_height * m_bpp);
}

void gx::Image::deserializeLZW(const gx::Serializer &s)
{
	gx::Lzw lzw;
	lzw.uncompressFrom(s, m_pixels, m_width * m_height * m_bpp);

	switch (m_format)
	{
		case GRAY_SCALE:
		case RGB_565:
		{
			// No filter
			break;
		}

		case RGB_888:
		{
			unfilterSurfaceRGB();
			break;
		}

		case RGBA_8888:
		{
			unfilterSurfaceRGBA();
			break;
		}
	}
}

void gx::Image::deserializeRLE(const gx::Serializer &s)
{
	switch (m_format)
	{
		case gx::Image::GRAY_SCALE:
		case gx::Image::RGB_888:
		case gx::Image::RGBA_8888:
		{
			deserializeRLE_8bits(s);
			break;
		}

		case gx::Image::RGB_565:
		{
			deserializeRLE_565(s);
			break;
		}
	}
}

void gx::Image::deserializeRLE_8bits(const gx::Serializer &s)
{
	size_t              sz;
	uint8_t             b;
	uint8_t             v;
	uint8_t             count;
	size_t              i;
	size_t              j;

	sz = m_width * m_height;

	for (uint8_t c = 0; c < m_bpp; c++)
	{
		i = 0;

		while (i < sz)
		{
			b = s.readByte();

			if (b == 'x')
			{
				// Count
				count = s.readByte();

				// Value
				v = s.readByte();

				for (j = 0; j < count; j++)
				{
					m_pixels[(i + j)*m_bpp + c] = v;
				}

				i += j;
			}
			else
			{
				// Normal value
				m_pixels[i * m_bpp + c] = b;
				i++;
			}
		}
	}
}

void gx::Image::deserializeRLE_565(const gx::Serializer &s)
{
	size_t    sz;
	uint16_t  b;
	uint16_t  v;
	uint8_t   count;
	size_t    i;
	size_t    j;
	uint16_t *surf;

	surf = (uint16_t *)m_pixels;
	sz = m_width * m_height;

	i = 0;

	while (i < sz)
	{
		b = s.readShort();

		if (b == 'x')
		{
			// Count
			count = s.readByte();

			// Value
			v = s.readShort();

			for (j = 0; j < count; j++)
			{
				surf[(i + j)] = v;
			}

			i += j;
		}
		else
		{
			// Normal value
			surf[i] = b;
			i++;
		}
	}
}


void gx::Image::deserializeRLELZW(const gx::Serializer &s)
{
	gx::Serializer tmp;
	gx::Lzw        lzw;
	uint32_t       sz;
	uint8_t       *buff;

	sz = s.readInt32();

	if (sz != 0)
	{
		buff = new uint8_t[sz];

		if (buff)
		{
			tmp.setStaticData(buff, sz);

			lzw.uncompressFrom(s, buff, sz);

			switch (m_format)
			{
				case gx::Image::GRAY_SCALE:
				case gx::Image::RGB_888:
				case gx::Image::RGBA_8888:
				{
					deserializeRLE_8bits(tmp);
					break;
				}

				case gx::Image::RGB_565:
				{
					deserializeRLE_565(tmp);
					break;
				}
			}

			delete buff;
		}
	}
}

void gx::Image::filterSurfaceRGBA() const
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
	uint8_t t;

	r = m_pixels[0];
	g = m_pixels[1];
	b = m_pixels[2];
	a = m_pixels[3];

	for (uint32_t i = 1; i < m_width * m_height ; i++)
	{
		t = m_pixels[i * m_bpp];
		m_pixels[i * m_bpp]   = t - r;
		r = t;

		t = m_pixels[i * m_bpp + 1];
		m_pixels[i * m_bpp + 1]   = t - g;
		g = t;

		t = m_pixels[i * m_bpp + 2];
		m_pixels[i * m_bpp + 2]   = t - b;
		b = t;

		t = m_pixels[i * m_bpp + 3];
		m_pixels[i * m_bpp + 3]   = t - a;
		a = t;
	}
}

void gx::Image::unfilterSurfaceRGBA() const
{
	for (uint32_t i = 1; i < m_width * m_height ; i++)
	{
		m_pixels[i * m_bpp] += m_pixels[(i - 1) * m_bpp];
		m_pixels[i * m_bpp + 1] += m_pixels[(i - 1) * m_bpp + 1];
		m_pixels[i * m_bpp + 2] += m_pixels[(i - 1) * m_bpp + 2];
		m_pixels[i * m_bpp + 3] += m_pixels[(i - 1) * m_bpp + 3];
	}
}

void gx::Image::filterSurfaceRGB() const
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t t;

	r = m_pixels[0];
	g = m_pixels[1];
	b = m_pixels[2];

	for (uint32_t i = 1; i < m_width * m_height ; i++)
	{
		t = m_pixels[i * m_bpp];
		m_pixels[i * m_bpp]   = t - r;
		r = t;

		t = m_pixels[i * m_bpp + 1];
		m_pixels[i * m_bpp + 1]   = t - g;
		g = t;

		t = m_pixels[i * m_bpp + 2];
		m_pixels[i * m_bpp + 2]   = t - b;
		b = t;
	}
}

void gx::Image::unfilterSurfaceRGB() const
{
	for (uint32_t i = 1; i < m_width * m_height ; i++)
	{
		m_pixels[i * m_bpp] += m_pixels[(i - 1) * m_bpp];
		m_pixels[i * m_bpp + 1] += m_pixels[(i - 1) * m_bpp + 1];
		m_pixels[i * m_bpp + 2] += m_pixels[(i - 1) * m_bpp + 2];
	}
}
