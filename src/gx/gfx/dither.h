/**************************************************************************
* Copyright (C) 2018 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file dither.h
* @author GORAND Charles
*
*/

#ifndef GX_DITHER_H
#define GX_DITHER_H

/* Dither Tresshold for Red Channel */
static const uint8_t dither_tresshold_r[64] =
{
	1, 7, 3, 5, 0, 8, 2, 6,
	7, 1, 5, 3, 8, 0, 6, 2,
	3, 5, 0, 8, 2, 6, 1, 7,
	5, 3, 8, 0, 6, 2, 7, 1,

	0, 8, 2, 6, 1, 7, 3, 5,
	8, 0, 6, 2, 7, 1, 5, 3,
	2, 6, 1, 7, 3, 5, 0, 8,
	6, 2, 7, 1, 5, 3, 8, 0
};

/* Dither Tresshold for Green Channel */
static const uint8_t dither_tresshold_g[64] =
{
	1, 3, 2, 2, 3, 1, 2, 2,
	2, 2, 0, 4, 2, 2, 4, 0,
	3, 1, 2, 2, 1, 3, 2, 2,
	2, 2, 4, 0, 2, 2, 0, 4,

	1, 3, 2, 2, 3, 1, 2, 2,
	2, 2, 0, 4, 2, 2, 4, 0,
	3, 1, 2, 2, 1, 3, 2, 2,
	2, 2, 4, 0, 2, 2, 0, 4
};

/* Dither Tresshold for Blue Channel */
static const uint8_t dither_tresshold_b[64] =
{
	5, 3, 8, 0, 6, 2, 7, 1,
	3, 5, 0, 8, 2, 6, 1, 7,
	8, 0, 6, 2, 7, 1, 5, 3,
	0, 8, 2, 6, 1, 7, 3, 5,

	6, 2, 7, 1, 5, 3, 8, 0,
	2, 6, 1, 7, 3, 5, 0, 8,
	7, 1, 5, 3, 8, 0, 6, 2,
	1, 7, 3, 5, 0, 8, 2, 6
};


#define DITHER_MIN(a,b) (((a)<(b))?(a):(b))
#define DITHER_MAX(a,b) (((a)>(b))?(a):(b))

/* red & blue */
#define  closest_rb(c) (c >> 3 << 3)
/* green */
#define closest_g(c) (c >> 2 << 2)

static inline uint16_t gx_dither_xy(uint32_t x, uint32_t y, uint16_t r, uint16_t g, uint16_t b)
{
	/* Get Tresshold Index */
	uint8_t tresshold_id = ((y & 7) << 3) + (x & 7);

	r = closest_rb(DITHER_MIN(r + dither_tresshold_r[tresshold_id], 0xff));
	g = closest_g(DITHER_MIN(g + dither_tresshold_g[tresshold_id], 0xff));
	b = closest_rb(DITHER_MIN(b + dither_tresshold_b[tresshold_id], 0xff));
	return ((r >> 3) << 11) | ((g >> 2) << 5) | (b >> 3) ;
}

#endif // ndef GX_DITHER_H

