/**************************************************************************
* Copyright (C) 2017 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file Log.h
* @author GORAND Charles
*
*/

#ifndef GX_LOG_H
#define GX_LOG_H

#ifndef GX_LOG_TAG
#define GX_LOG_TAG "UNDEFINED GX_LOG_TAG"
#endif // ndef GX_LOG_TAG

#include "gx/core/defines.h"

namespace gx
{

// FIXME : Shall be removed
#define GX_LOG_ENABLED

#ifdef GX_LOG_ENABLED
#define GX_LOG(FMT,...) gx::Log::log(GX_LOG_TAG,FMT, ##__VA_ARGS__)
#define GX_WARN(FMT,...) gx::Log::warn(GX_LOG_TAG,FMT, ##__VA_ARGS__)
#define GX_ERROR(FMT,...) gx::Log::error(GX_LOG_TAG,__FILE__,__LINE__,FMT, ##__VA_ARGS__)
#define GX_FATAL(FMT,...) gx::Log::fatal(GX_LOG_TAG,__FILE__,__LINE__,FMT, ##__VA_ARGS__)
#define GX_HEXDUMP(buf,size,FMT, ...) gx::Log::hexdump(GX_LOG_TAG,buf,size,FMT, ##__VA_ARGS__);
#else
#define GX_LOG(FMT,...)
#define GX_WARN(FMT,...)
#define GX_ERROR(FMT,...)
#define GX_FATAL(FMT,...)
#define GX_HEXDUMP(buf,size)
#endif // def GX_LOG_ENABLED

class Log
{
	public:
		static void log(const char* tag,const char* str,...);
		static void warn(const char* tag,const char* str,...);
		static void error(const char* tag,const char* file,const int line,const char* str,...);
		static void fatal(const char* tag,const char* file,const int line,const char* str,...);
		static void hexdump(const char* tag,const uint8_t *buffer, size_t size, const char* str,...);

	private:
		Log();
		virtual ~Log();
};

};

#endif // ndef GX_LOG_H
