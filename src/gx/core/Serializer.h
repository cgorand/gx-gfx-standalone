/**************************************************************************
* Copyright (C) 2017 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file Serializer.h
* @author GORAND Charles
*
*/

#ifndef GX_SERIALIZER_H
#define GX_SERIALIZER_H

#include "gx/core/defines.h"

namespace gx
{

class Serializer
{
	public:
		Serializer();
		Serializer(uint8_t *buffer, size_t sz);
		Serializer(const uint8_t *buffer, size_t sz);
		virtual ~Serializer();

		void setStaticData(uint8_t *buffer, size_t sz);
		void setStaticROData(const uint8_t *buffer, size_t sz);
		void copyData(const uint8_t *buffer, size_t sz);
		void copyFrom(const Serializer &source);

		void clear();
		const uint8_t *dataAtCurrentPosition() const;
		void forward(size_t nbBytes) const;
		void seekEnd() const;
		void skipFirstBytes(size_t nbBytes);
		const uint8_t *getData() const;
		size_t getRemainingDataSize() const;
		size_t getSize() const;
		size_t getCapacity() const;
		uint8_t readByte() const;
		float readFloat() const;
		uint32_t readInt32() const;
		void read(void *outBuffer, size_t len) const;
		uint16_t readShort() const;
		const char *readString()const;
		void rewind()const;
		void writeByte(uint8_t i);
		void writeFloat(float f);
		void writeInt32(uint32_t i);
		void write(const void *buffer, size_t len);
		void writeShort(uint16_t i);
		void writeString(const char *s);

	private:
		void growData(size_t len);

	private:
		uint8_t        *m_data;
		const uint8_t  *m_constData;
		size_t          m_dataLength;
		size_t          m_curLength;
		mutable size_t  m_curPosition;
		bool            m_isReadOnly;
		bool            m_isDynamic;
};
};


#endif // ndef GX_SERIALIZER_H

