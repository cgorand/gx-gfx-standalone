/**************************************************************************
* Copyright (C) 2017 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file Serializer.cpp
* @author GORAND Charles
*
*/

#include "Serializer.h"

#define GX_LOG_TAG "Serializer"
#include "gx/core/Log.h"

#include <string.h>

gx::Serializer::Serializer()
{
	m_data        = new uint8_t[16];
	m_constData   = nullptr;
	m_dataLength  = 16;
	m_curLength   = 0;
	m_curPosition = 0;
	m_isReadOnly  = false;
	m_isDynamic   = true;
}

gx::Serializer::Serializer(uint8_t *buffer, size_t sz)
{
	setStaticData(buffer, sz);
}

gx::Serializer::Serializer(const uint8_t *buffer, size_t sz)
{
	setStaticROData(buffer, sz);
}

gx::Serializer::~Serializer()
{
	if (m_isDynamic)
	{
		if (m_data)
		{
			delete m_data;
		}
	}
}

void gx::Serializer::copyData(const uint8_t* buffer, size_t sz)
{
	if (m_isDynamic)
	{
		if (m_data)
		{
			delete m_data;
		}
	}

	m_data        = new uint8_t[sz];
	m_constData   = nullptr;
	m_dataLength  = sz;
	m_curLength   = sz;
	m_curPosition = 0;
	m_isReadOnly  = false;
	m_isDynamic   = true;

	memcpy(m_data, buffer, sz);
}

void gx::Serializer::copyFrom(const gx::Serializer& source)
{
	copyData(source.getData(), source.getSize());
}

void gx::Serializer::setStaticData(uint8_t* buffer, size_t sz)
{
	m_data        = buffer;
	m_constData   = nullptr;
	m_dataLength  = sz;
	m_curLength   = sz;
	m_curPosition = 0;
	m_isReadOnly  = false;
	m_isDynamic   = false;
}

void gx::Serializer::setStaticROData(const uint8_t* buffer, size_t sz)
{
	m_data        = nullptr;
	m_constData   = buffer;
	m_dataLength  = sz;
	m_curLength   = sz;
	m_curPosition = 0;
	m_isReadOnly  = true;
	m_isDynamic   = false;
}


void gx::Serializer::clear()
{
	m_curPosition = 0;
	m_curLength   = 0;
}

const uint8_t* gx::Serializer::dataAtCurrentPosition() const
{
	if (m_isReadOnly)
	{
		return &(m_constData[m_curPosition]);
	}
	else
	{
		return &(m_data[m_curPosition]);
	}
}

void gx::Serializer::forward(size_t nbBytes)  const
{
	if (m_curPosition + nbBytes <= m_dataLength)
	{
		m_curPosition += nbBytes;
	}
}

void gx::Serializer::seekEnd() const
{
	m_curPosition = m_curLength;
}

void gx::Serializer::skipFirstBytes(size_t nbBytes)
{
	if ((m_curLength > 0) && (nbBytes <= m_curLength))
	{
		m_curLength -= nbBytes;
		memmove(m_data, m_data + nbBytes, m_curLength);
	}
}

const uint8_t *gx::Serializer::getData()  const
{
	if (m_isReadOnly)
	{
		return m_constData;
	}
	else
	{
		return m_data;
	}
}

size_t gx::Serializer::getRemainingDataSize()  const
{
	return m_curLength - m_curPosition;
}

size_t gx::Serializer::getSize()  const
{
	return m_curLength;
}

size_t gx::Serializer::getCapacity()  const
{
	return m_dataLength;
}

uint8_t gx::Serializer::readByte() const
{
	const uint8_t *ptr;

	if (m_isReadOnly)
	{
		ptr = m_constData;
	}
	else
	{
		ptr = m_data;
	}

	if (m_curPosition < m_dataLength)
	{
		uint8_t result         = ptr[m_curPosition];
		m_curPosition         += sizeof(uint8_t);
		return result;
	}

	return 0;
}

float gx::Serializer::readFloat() const
{
	const uint8_t *ptr;

	if (m_isReadOnly)
	{
		ptr = m_constData;
	}
	else
	{
		ptr = m_data;
	}

	if (m_curPosition < m_dataLength)
	{
		uint32_t i0    = ptr[m_curPosition];
		uint32_t i1    = ptr[m_curPosition + 1];
		uint32_t i2    = ptr[m_curPosition + 2];
		uint32_t i3    = ptr[m_curPosition + 3];
		m_curPosition += sizeof(float);
		return (float)((i3 << 24) | (i2 << 16) | (i1 << 8) | i0);
	}

	return 0;
}

uint32_t gx::Serializer::readInt32() const
{
	const uint8_t *ptr;

	if (m_isReadOnly)
	{
		ptr = m_constData;
	}
	else
	{
		ptr = m_data;
	}

	if (m_curPosition < m_dataLength)
	{
		uint32_t i0    = ptr[m_curPosition];
		uint32_t i1    = ptr[m_curPosition + 1];
		uint32_t i2    = ptr[m_curPosition + 2];
		uint32_t i3    = ptr[m_curPosition + 3];
		m_curPosition += sizeof(uint32_t);
		return ((i3 << 24) | (i2 << 16) | (i1 << 8) | i0);
	}

	return 0;
}

void gx::Serializer::read(void *outBuffer, size_t len) const
{
	const uint8_t *ptr;

	if (m_isReadOnly)
	{
		ptr = m_constData;
	}
	else
	{
		ptr = m_data;
	}

	if (!outBuffer)
	{
		return;
	}

	if (m_curPosition < m_dataLength)
	{
		const uint8_t *start = ptr + m_curPosition;
		memcpy(outBuffer, start, len);
		m_curPosition += len;
	}
}

uint16_t gx::Serializer::readShort()  const
{
	const uint8_t *ptr;

	if (m_isReadOnly)
	{
		ptr = m_constData;
	}
	else
	{
		ptr = m_data;
	}

	if (m_curPosition < m_dataLength)
	{
		uint16_t rSH           = ptr[m_curPosition + 1];
		uint16_t rSL           = ptr[m_curPosition];
		m_curPosition += sizeof(uint16_t);
		return ((rSH << 8) | rSL);
	}

	return 0;
}

const char *gx::Serializer::readString() const
{
	const uint8_t *ptr;

	if (m_isReadOnly)
	{
		ptr = m_constData;
	}
	else
	{
		ptr = m_data;
	}

	if (m_curPosition < m_dataLength)
	{
		char *result   = (char *)(ptr + m_curPosition);
		m_curPosition += strlen(result) + 1;
		return result;
	}

	return NULL;
}

void gx::Serializer::rewind() const
{
	m_curPosition = 0;
}

void gx::Serializer::growData(size_t len)
{
	if (m_isDynamic)
	{
		if ((m_curPosition + len) < m_dataLength)
			return;

		uint32_t newSize = m_dataLength + (m_dataLength >> 1) + len;

		uint8_t *newBuffer = new uint8_t[newSize];

		memcpy(newBuffer, m_data, m_dataLength);

		delete m_data;

		m_data = newBuffer;
		m_dataLength = newSize;
	}
}


void gx::Serializer::writeByte(uint8_t i)
{
	if (m_isReadOnly)
	{
		GX_ERROR("Read only");
		return;
	}

	growData(sizeof(uint8_t));

	if ((m_curPosition + sizeof(uint8_t)) <= m_dataLength)
	{
		m_data[m_curPosition] = i;
		m_curPosition += sizeof(uint8_t);
		m_curLength   += sizeof(uint8_t);
	}
	else
	{
		GX_ERROR("Full : not dynamic");
	}
}

void gx::Serializer::writeFloat(float f)
{
	if (m_isReadOnly)
	{
		GX_ERROR("Read only");
		return;
	}

	growData(sizeof(float));

	uint32_t i;
	uint32_t i0;
	uint32_t i1;
	uint32_t i2;
	uint32_t i3;

	memcpy((uint8_t *)&i, (uint8_t *)&f, sizeof(uint32_t));

	i0 = i & 0xFF;
	i1 = (i >> 8) & 0xFF;
	i2 = (i >> 16) & 0xFF;
	i3 = (i >> 24) & 0xFF;

	if ((m_curPosition + sizeof(float)) <= m_dataLength)
	{
		m_data[m_curPosition]     = i0;
		m_data[m_curPosition + 1] = i1;
		m_data[m_curPosition + 2] = i2;
		m_data[m_curPosition + 3] = i3;

		m_curPosition += sizeof(float);
		m_curLength   += sizeof(float);
	}
	else
	{
		GX_ERROR("Full : not dynamic");
	}
}

void gx::Serializer::writeInt32(uint32_t i)
{
	if (m_isReadOnly)
	{
		GX_ERROR("Read only");
		return;
	}

	growData(sizeof(uint32_t));

	uint32_t i0 = i & 0xFF;
	uint32_t i1 = (i >> 8) & 0xFF;
	uint32_t i2 = (i >> 16) & 0xFF;
	uint32_t i3 = (i >> 24) & 0xFF;

	if ((m_curPosition + sizeof(uint32_t)) <= m_dataLength)
	{
		m_data[m_curPosition]     = i0;
		m_data[m_curPosition + 1] = i1;
		m_data[m_curPosition + 2] = i2;
		m_data[m_curPosition + 3] = i3;
		m_curPosition            += sizeof(uint32_t);
		m_curLength              += sizeof(uint32_t);
	}
	else
	{
		GX_ERROR("Full : not dynamic");
	}
}

void gx::Serializer::write(const void* buffer, size_t len)
{
	if (m_isReadOnly)
	{
		GX_ERROR("Read only");
		return;
	}

	growData(len);

	if ((m_curPosition + len) <= m_dataLength)
	{
		memcpy((m_data + m_curPosition), buffer, len);
		m_curPosition += len;
		m_curLength   += len;
	}else
	{
		GX_ERROR("Full : not dynamic");
	}
}

void gx::Serializer::writeShort(uint16_t i)
{
	if (m_isReadOnly)
	{
		GX_ERROR("Read only");
		return;
	}

	growData(sizeof(uint16_t));
	
	uint16_t rSH = (i >> 8) & 0xFF;
	uint16_t rSL = (i & 0xFF);

	if ((m_curPosition + sizeof(uint16_t)) <= m_dataLength)
	{
		m_data[m_curPosition]     = rSL;
		m_data[m_curPosition + 1] = rSH;
		m_curPosition            += sizeof(uint16_t);
		m_curLength              += sizeof(uint16_t);
	}else
	{
		GX_ERROR("Full : not dynamic");
	}
}

void gx::Serializer::writeString(const char* s)
{
	if (m_isReadOnly)
	{
		GX_ERROR("Read only");
		return;
	}

	growData(strlen(s)+1);

	if ((m_curPosition + strlen(s) + 1) <= m_dataLength)
	{
		strcpy((char *)(m_data + m_curPosition), s);
		m_curPosition += strlen(s) + 1;
		m_curLength   += strlen(s) + 1;
	}else
	{
		GX_ERROR("Full : not dynamic");
	}
}



