/**************************************************************************
* Copyright (C) 2017 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file Lzw.h
* @author GORAND Charles
*
*/

#ifndef GX_LZW_H
#define GX_LZW_H

#include "gx/core/Serializer.h"

void lzw_comp_write_buff(int value, void *context);
int lzw_comp_read_buff(void *context);

namespace gx
{
class Lzw
{
	public:
		enum eResult
		{
			SUCCESS,
			FAILED
		};

		//   level      encoder RAM   decoder RAM
		//              requirement   requirement
		// -----------------------------------------
		//   LOW      1792 bytes    1024 bytes
		//  MEDIUM    4352 bytes    3072 bytes
		//   HIGH     9472 bytes    7168 bytes
		//  ULTRA     19712 bytes   15360 bytes
		enum eCompression
		{
			LOW,
			MEDIUM,
			HIGH,
			ULTRA,
		};

		Lzw();
		virtual ~Lzw();

		eResult compressTo(eCompression level,const uint8_t *inBuffer, size_t sz, Serializer &output);
		eResult uncompressFrom(const Serializer &input, uint8_t *outBuffer, size_t sz);
	private:
		friend void ::lzw_comp_write_buff(int value, void *context);
		friend int ::lzw_comp_read_buff(void *context);
	private:
		uint8_t convertCompressionLevel(eCompression level);

	private:
		Serializer       *m_writeTo;
		const Serializer *m_readFrom;
};
};

#endif // ndef GX_LZW_H

