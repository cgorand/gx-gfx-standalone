/**************************************************************************
* Copyright (C) 2017 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file Lzw.cpp
* @author GORAND Charles
*
*/

#include "Lzw.h"

#include "lzw/lzw-lib.h"

#define GX_LOG_TAG "Lzw"
#include "gx/core/Log.h"

void lzw_comp_write_buff (int value, void* context)
{
	gx::Lzw* thiz = (gx::Lzw*) context;
	thiz->m_writeTo->writeByte(value);
}

int lzw_comp_read_buff (void* context)
{
	gx::Lzw* thiz = (gx::Lzw*) context;

	if (thiz->m_readFrom->getRemainingDataSize() > 0)
	{
		return thiz->m_readFrom->readByte();
	}else
	{
		return -1;
	}
}

gx::Lzw::Lzw()
{
	m_writeTo  = nullptr;
	m_readFrom = nullptr;
}

gx::Lzw::~Lzw()
{
}

gx::Lzw::eResult gx::Lzw::compressTo(gx::Lzw::eCompression level,const uint8_t* inBuffer, size_t sz, gx::Serializer& output)
{
	int res;
	gx::Lzw::eResult eRes = gx::Lzw::SUCCESS;

	m_readFrom = new gx::Serializer(inBuffer, sz);
	m_writeTo  = &output;

	res = lzw_compress(lzw_comp_write_buff, lzw_comp_read_buff, convertCompressionLevel(level), this);

	delete m_readFrom;
	m_readFrom = nullptr;
	m_writeTo  = nullptr;

	if (res)
	{
		eRes = gx::Lzw::FAILED;
	}
	return eRes;
}

gx::Lzw::eResult gx::Lzw::uncompressFrom(const gx::Serializer& input, uint8_t* outBuffer, size_t sz)
{
	int res;
	gx::Lzw::eResult eRes = gx::Lzw::SUCCESS;

	m_readFrom = &input;
	m_writeTo  = new gx::Serializer(outBuffer, sz);

	res = lzw_decompress(lzw_comp_write_buff, lzw_comp_read_buff, this);

	delete m_writeTo;
	m_readFrom = nullptr;
	m_writeTo  = nullptr;

	if (res)
	{
		eRes = gx::Lzw::FAILED;
	}
	return eRes;
}

uint8_t gx::Lzw::convertCompressionLevel(gx::Lzw::eCompression level)
{
	switch (level)
	{
		case LOW:
		{
			return 9;
		}
		case MEDIUM:
		{
			return 10;
		}
		case HIGH:
		{
			return 11;
		}
		case ULTRA:
		{
			return 12;
		}
	}
	return 9;
}

