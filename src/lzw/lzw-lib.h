////////////////////////////////////////////////////////////////////////////
//                            **** LZW-AB ****                            //
//               Adjusted Binary LZW Compressor/Decompressor              //
//                     Copyright (c) 2016 David Bryant                    //
//                           All Rights Reserved                          //
//      Distributed under the BSD Software License (see license.txt)      //
////////////////////////////////////////////////////////////////////////////

#ifndef LZWLIB_H_
#define LZWLIB_H_

typedef void (*lzw_dst_cb)(int, void* context);
typedef int  (*lzw_src_cb)(void* context);

int lzw_compress (lzw_dst_cb dst, lzw_src_cb src, int maxbits, void* context);
int lzw_decompress (lzw_dst_cb dst, lzw_src_cb src, void* context);

#endif /* LZWLIB_H_ */
