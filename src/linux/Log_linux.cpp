/**************************************************************************
* Copyright (C) 2017 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file Log.cpp
* @author GORAND Charles
*
*/

#include "gx/core/Log.h"

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <sys/time.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>

#define COL(x)    "\033[1;" #x "m"
#define RED       COL(31)
#define GREEN     COL(32)
#define YELLOW    COL(33)
#define BLUE      COL(34)
#define LBLUE     COL(36)
#define BOLDGRAY  COL(39)
#define WHITE     COL(37)
#define GRAY      "\033[0m"

static char       gTimeStr[10];
static char       gTmpBuff[34];

void log_getDate()
{
	struct tm *ptr;
	time_t tm;
	tm = time(NULL);
	ptr = localtime(&tm);
	sprintf(gTimeStr, "%.2i:%.2i:%.2i", ptr->tm_hour, ptr->tm_min, ptr->tm_sec);
}

void gx::Log::log(const char *tag, const char *str, ...)
{
	va_list  ap;
	va_start(ap, str);

	log_getDate();

	printf(BOLDGRAY"[" LBLUE "%s" BOLDGRAY "]" WHITE ">" BOLDGRAY "[" GREEN "%s" BOLDGRAY "]" WHITE " : ", gTimeStr, tag);
	vprintf(str, ap);
	printf("\n" GRAY);
}

void gx::Log::warn(const char *tag, const char *str, ...)
{
	va_list  ap;
	va_start(ap, str);

	log_getDate();

	printf(BOLDGRAY "[" LBLUE "%s" BOLDGRAY "]" WHITE ">" BOLDGRAY "[" YELLOW "%s" BOLDGRAY "]" WHITE " : ", gTimeStr, tag);
	vprintf(str, ap);
	printf("\n" GRAY);
}

void gx::Log::error(const char *tag, const char *file, const int line, const char *str, ...)
{
	va_list  ap;
	va_start(ap, str);

	log_getDate();

	printf(BOLDGRAY "[" LBLUE "%s" BOLDGRAY "]" WHITE ">" BOLDGRAY "[" RED "%s" BOLDGRAY "]" WHITE " : " RED "ERROR" WHITE " : " YELLOW "%s" WHITE " line " YELLOW "%i" WHITE " : ", gTimeStr, tag, file, line);
	vprintf(str, ap);
	printf("\n" GRAY);
}

void gx::Log::fatal(const char *tag, const char *file, const int line, const char *str, ...)
{
	va_list  ap;
	va_start(ap, str);

	log_getDate();

	printf(BOLDGRAY "[" LBLUE "%s" BOLDGRAY "]" WHITE ">" BOLDGRAY "[" RED "%s" BOLDGRAY "]" WHITE " : " RED "FATAL" WHITE " : " YELLOW "%s" WHITE " line " YELLOW "%i" WHITE " : ", gTimeStr, tag, file, line);
	vprintf(str, ap);
	printf("\n" GRAY);
	asm("int $0x03");
}

void gx::Log::hexdump(const char *tag, const uint8_t *buffer, size_t size, const char *str, ...)
{
	uint8_t  *dataBuffer = (uint8_t *)buffer;
	uint32_t  i         = 0;
	uint32_t  buffIter  = 0;
	uint32_t  tmpI      = 0;
	va_list   ap;

	va_start(ap, str);

	while (i < size)
	{
		buffIter = 0;
		tmpI     = i;

		while ((tmpI < size) && (buffIter < 24))
		{
			sprintf(gTmpBuff + buffIter, "%.2X ", dataBuffer[tmpI]);
			buffIter += 3;
			tmpI++;
		}

		tmpI = i;

		while ((tmpI < size) && (buffIter < 32))
		{
			if (dataBuffer[tmpI] >= 32)
			{
				gTmpBuff[buffIter++] = dataBuffer[tmpI];
			}
			else
			{
				gTmpBuff[buffIter++] = '.';
			}

			tmpI++;
		}

		i = tmpI;
		gTmpBuff[buffIter++] = 0;

		log_getDate();

		printf(BOLDGRAY "[" LBLUE "%s" BOLDGRAY "]" WHITE ">" BOLDGRAY "[" BLUE "%s" BOLDGRAY "]" WHITE " : ", gTimeStr, tag);
		vprintf(str, ap);
		printf("%s",gTmpBuff);
		printf("\n" GRAY);
	}
}
