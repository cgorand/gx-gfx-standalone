# gx-gfx : A simple image compression library (standalone)

## What is it ?

This library was developped by me with other libraries "gx-*" (or gears) which is a set of libraries for embedded system.
I never publish the "gx libraries". But inside it, there is this module that I adapt to make it standalone.

Using libpng or libjpeg for embedded system is not always a suitable solution as it requires CPU, flash memory and RAM.
This library can use different algorithm to compress an Image and decompress it later on embedded device.

This library use the following (free) algorithm to compress images :

* NONE : No compression
* RLE_PLAIN : Run Length Encoding
* RLE_LZW : Run Length Encoding with after LZW (Lempel Ziv Welch)
* LZW : Lempel Ziv Welch compression. Image is filtered first for RGB and RGBA.
* BEST : Select the best algorithm to compress image.

NOTE : If image is RGB 565, BEST algorithm try to compress the image in RGB 565, and in RGB 888.
It happens that compressing the image RGB 888 get better results than in 565. But the decompression
while also use more memory.

It supports the following image format :

* Gray scale
* RGB 565
* RGB 888
* RGBA 8888

The library has been written in C++ without using STL.

## Requirements

The library has been tested on Debian and has been used in embedded system too with good performances.

The library needs at least a memory allocator available (new/delete) to be able to work and enought RAM to work.
This quantity of RAM memory is related to screen size, images size and images format.

## How to use ?

Clone the repository : 
```
git@gitlab.com:cgorand/gx-gfx-example.git
```
And check read README.md for more details.


## Performances

Most of the compression is lossless except if you convert RGB(A) images into RGB 565. A dithering is applied on compressed image.
With BEST algorithm, the compression reach results similare to PNG. Sometimes, it's worth and sometimes it's better.
Compression shall be done on a computer, it's not intented to be run on embedded system.

## TODO

The next update of the library shall be able to draw directly from flash on screen without using RAM buffer.

## License

This library use LZW code from [lzw-ab](https://github.com/dbry/lzw-ab)
