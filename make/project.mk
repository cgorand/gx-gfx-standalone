###########################################################
# Project definitions
###########################################################

#Define binary output name. If library don't add extension. It can be configured in pf/*.mk
OUTPUT_NAME=gx-gfx-standalone

# Select output type
# OUTPUT_TYPE=binary
OUTPUT_TYPE=staticlib
# OUTPUT_TYPE=sharedlib

#C++ Symbols are not found if static lib is stripped.
#STRIP_SYMBOL=1

include gcc.mk
include gfx.mk

