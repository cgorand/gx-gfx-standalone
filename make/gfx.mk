###########################################################################
# Copyright (C) 2017 GORAND Charles
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# Developped by Charles GORAND : charles.gorand.dev@gmail.com
# 
############################################################################

###########################################################
# APPLICATION Sources
###########################################################
###########################################################
# GFX Sources
###########################################################

GFX_INCLUDES+=\
	$(call getModuleDefaultInclude,gx-core) \
	$(call getModuleDefaultInclude,gx-lzw)
GFX_CFLAGS+=
GFX_LDFLAGS+=
GFX_CPP+= \
	gx/gfx/Image.cpp \
	gx/gfx/ImageConverterRGB.cpp \
	gx/gfx/ImageConverter565.cpp \
	gx/gfx/ImageConverterGrayScale.cpp \
	gx/core/Serializer.cpp \
	gx/core/Serializable.cpp \
	gx/core/Log.cpp \
	gx/lzw/Lzw.cpp \
	lzw/lzw-lib.cpp \
	linux/Log_linux.cpp

GFX_C+=
GFX_ASM+=
GFX_MODULES+=

###########################################################
# Update project configuration
###########################################################
PROJECT_INCLUDES+=$(GFX_INCLUDES)
PROJECT_CFLAGS+=$(GFX_CFLAGS)
PROJECT_LDFLAGS+=$(GFX_LDFLAGS)
PROJECT_CPP_FILES+=$(GFX_CPP)
PROJECT_C_FILES+=$(GFX_C)
PROJECT_ASM_FILES+=$(GFX_ASM)
PROJECT_MODULES+=$(GFX_MODULES)
